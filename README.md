# IDS-721-Cloud-Computing :computer:

## Individual Project 2 :page_facing_up: 

## :ballot_box_with_check: Requirements
* Simple REST API/web service in Rust
* Dockerfile to containerize service
* CI/CD pipeline files

## :ballot_box_with_check: Grading Criteria
* __Rust Microservice Functionality__ 30 points
* __Docker Configuration__ 20 points
* __CI/CD Pipeline__ 30 points
* __Documentation__ 10 points
* __Demo Video__ 10 points

## :ballot_box_with_check: Main Progress
1. __`Simple REST API/web service in Rust`__: To create a simple REST API/web service in Rust, I have implemented a straightforward function that accepts the quantities of strawberries and bananas as parameters in the URL. It then calculates the total price by multiplying the quantities by the respective unit prices of each fruit.

```rust
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::Deserialize;

// Derive Deserialize for FruitQuantities
#[derive(Deserialize)]
struct FruitQuantities {
    strawberries: u32,
    bananas: u32,
}

async fn calculate_fruit_prices_in_usd(quantities: web::Path<FruitQuantities>) -> impl Responder {
    let price_per_strawberry_usd: f32 = 0.50; // Price per strawberry in USD
    let price_per_banana_usd: f32 = 0.30; // Price per banana in USD

    let total_price_strawberries_usd = price_per_strawberry_usd * quantities.strawberries as f32;
    let total_price_bananas_usd = price_per_banana_usd * quantities.bananas as f32;
    let total_price_usd = total_price_strawberries_usd + total_price_bananas_usd;

    HttpResponse::Ok().body(format!("The total price for {} strawberries and {} bananas is ${:.2} USD.", quantities.strawberries, quantities.bananas, total_price_usd))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/strawberries/{strawberries}/bananas/{bananas}", web::get().to(calculate_fruit_prices_in_usd))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
```
- Utilizes the `actix-web` framework for creating a web server.
- Defines a `FruitQuantities` struct to deserialize request parameters for strawberries and bananas.
- Implements an asynchronous function `calculate_fruit_prices_in_usd` to compute total cost.
- Uses dynamic URL paths to capture fruit quantities directly from the route.
- Calculates total price based on fixed USD rates per fruit.
- Returns a formatted string displaying the total price in an `HttpResponse::Ok`.

2. __`Dockerfile to containerize service`__: By containerizing the service with a Dockerfile and pushing the image to Docker Hub, the function written for port 8080 is exposed as a REST API/web service accessible through the container.

(1) __Build the Image__: Prepare to build the Docker image for the repository.
- Push a "latest" tagged image to an empty repository.
```bash
λ docker tag nginx:latest suimpark825/individual-project-2:latest
```
- Push the image to the Docker repository.
```bash
λ docker push suimpark825/individual-project-2:latest
```
![Docker](https://github.com/suim-park/Mini-Project-1/assets/143478016/3f575bdd-3baf-437a-bb3d-5d3f0a37a916)

(2) __Create the Dockerfile__: This Dockerfile sets up a Rust development environment to compile a simple web service. It first uses the latest Rust image as a base to build the application. Then, it switches to the lightweight Bitnami Minideb image to create the final container. The web service is configured to run on port 8080 and serves requests using the Rocket framework. Finally, it defines environment variables to specify the service's address and port, and launches the application within the container.

- Dockerfile:
```Dockerfile
# Set base image
FROM rust:latest as builder

# Set working directory
WORKDIR /usr/src/microservice

# Copy Rust application into the container
COPY . .

# Build the application
RUN cargo build --release

# Set a new base image
FROM bitnami/minideb:bookworm

# Set working directory
WORKDIR /usr/src/microservice

# Change the executable to the built one
COPY --from=builder /usr/src/microservice/target/release/microservice /usr/src/microservice

# Set environment variables
ENV ROCKET_ADDRESS=0.0.0.0
ENV ROCKET_PORT=8080

# Expose port
EXPOSE 8080

# Run the application
CMD ["./microservice"]
```
- Build the Docker image and push it to the GitLab repository.
```bash
λ docker build -t microservice .
```

(3) __API/Web Service__: After adding the Dockerfile, you can check the total price of strawberries and bananas based on their quantities on localhost at port 8080. Below are screenshots of the result screens showing the total price of fruit for different quantities of strawberries and bananas entered in the address.

- Strawberry: 10 / Bananas: 20 <br/>
(URL: http://localhost:8080/strawberries/10/bananas/20)
![Result-1](https://github.com/suim-park/Mini-Project-1/assets/143478016/edc88d80-204f-440e-bfa3-25f09a14e80e)

- Strawberry: 7 / Bananas: 9 <br/>
(URL: http://localhost:8080/strawberries/7/bananas/9)
![Result-2](https://github.com/suim-park/Mini-Project-1/assets/143478016/bc897c51-f1c0-4192-af7f-8e97361ee8df)


3. __`CI/CD pipeline files`__: The YAML file configures a CI/CD pipeline with `build` and `test` stages. In the build stage, it sets the Docker image tag, builds the Docker image, runs it as a container, and lists containers. This stage runs for the "main" branch and merge requests. In the "test" stage, it tests the code using the Cargo test suite. Both stages ensure consistent and reliable deployment.

```yaml
stages:
  - build
  - test

variables:
  IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_PROJECT_PATH:$CI_COMMIT_REF_SLUG

build:
  stage: build
  image: docker:stable
  services:
    - docker:dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t microservice .
    - docker run -d -p 8080:8080 microservice
    - docker ps -a
  only:
    - main
    - merge_requests

test:
  stage: test
  image: rust:latest
  script:
    - cargo test --verbose
  only:
    - main
    - merge_requests
```

- The CI/CD YAML file successfully builds and tests the code.
![Pipeline](https://github.com/suim-park/Mini-Project-1/assets/143478016/81dca7b3-9346-490d-b105-13b351620dc6)


## :movie_camera: Demo Video
:link: [Demo Video](https://youtu.be/MV8kixZHwi0)