use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::Deserialize;

// Derive Deserialize for FruitQuantities
#[derive(Deserialize)]
struct FruitQuantities {
    strawberries: u32,
    bananas: u32,
}

async fn calculate_fruit_prices_in_usd(quantities: web::Path<FruitQuantities>) -> impl Responder {
    let price_per_strawberry_usd: f32 = 0.50; // Price per strawberry in USD
    let price_per_banana_usd: f32 = 0.30; // Price per banana in USD

    let total_price_strawberries_usd = price_per_strawberry_usd * quantities.strawberries as f32;
    let total_price_bananas_usd = price_per_banana_usd * quantities.bananas as f32;
    let total_price_usd = total_price_strawberries_usd + total_price_bananas_usd;

    HttpResponse::Ok().body(format!("The total price for {} strawberries and {} bananas is ${:.2} USD.", quantities.strawberries, quantities.bananas, total_price_usd))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/strawberries/{strawberries}/bananas/{bananas}", web::get().to(calculate_fruit_prices_in_usd))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}