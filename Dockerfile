# Set base image
FROM rust:latest as builder

# Set working directory
WORKDIR /usr/src/microservice

# Copy Rust application into the container
COPY . .

# Build the application
RUN cargo build --release

# Set a new base image
FROM bitnami/minideb:bookworm

# Set working directory
WORKDIR /usr/src/microservice

# Change the executable to the built one
COPY --from=builder /usr/src/microservice/target/release/microservice /usr/src/microservice

# Set environment variables
ENV ROCKET_ADDRESS=0.0.0.0
ENV ROCKET_PORT=8080

# Expose port
EXPOSE 8080

# Run the application
CMD ["./microservice"]